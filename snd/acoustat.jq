# read acoustat JSON object as exported from R
# create ElasticSearch _bulk API JSON object to update record
{ 
    update: {
        _index: "wmmsdb", 
        _type: "record", 
        _id: .id[0] 
    } 
},
{
    doc: {
        sound: {
            freq: {
                IPR: .["freq.IPR"][0],
                M: .["freq.M"][0],
                P1: .["freq.P1"][0],
                P2: .["freq.P2"][0]
            },
            time: {
                IPR: .["time.IPR"][0],
                M: .["time.M"][0],
                P1: .["time.P1"][0],
                P2: .["time.P2"][0]
            }
        }
    }
}
