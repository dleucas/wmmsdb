#!/usr/bin/Rscript --vanilla
library("seewave")
library("tuneR")
argv = commandArgs(trailingOnly = TRUE)
wav = tuneR::readWave(argv[1])
png(filename =  argv[2], height = 611, width = 944)
seewave::spectro(wave=wav, wl=1024, wn="hamming", fftw=TRUE, scale = FALSE)
invisible(dev.off())
