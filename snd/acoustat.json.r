#!/usr/bin/Rscript --vanilla
suppressPackageStartupMessages({
library("seewave")
library("tuneR")
library("jsonlite")
library("methods")
})
argv = commandArgs(trailingOnly = TRUE)
wav = tuneR::readWave(argv[1])
stat = seewave::acoustat(wave=wav, plot = FALSE)
stat$freq.contour <- NULL
stat$time.contour <- NULL
stat$id <- argv[3]
write_json(stat, argv[2])
