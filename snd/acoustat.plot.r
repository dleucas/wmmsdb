#!/usr/bin/Rscript --vanilla
library("seewave")
library("tuneR")
argv = commandArgs(trailingOnly = TRUE)
wav = tuneR::readWave(argv[1])
png(filename =  argv[2], height = 640, width = 640)
seewave::acoustat(wave=wav)
invisible(dev.off())
