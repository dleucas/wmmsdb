# for each record create a ElasticSearch _bulk import action with _id
.| { "index" : { "_index" : "wmmsdb", 
                 "_type" : "record", 
                 "_id" : .record_number } },
# and the record itself
.
