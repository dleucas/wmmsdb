# Watkins Marine Mammal Sound Database, Woods Hole Oceanographic Institution

## Remastered Deluxe Edition

- [Data Source](http://cis.whoi.edu/science/B/whalesounds/fullCuts.cfm)
- [Source Code](https://codeberg.org/dleucas/wmmsdb)
- [Source Code (Mirror)](https://rocketgit.com/user/dleucas/wmmsdb)
- [Search Interface](https://marine-mammal.soundwave.cl)

# Overall Goal

Make the Sound Database more accessible and useful for researchers

- Download all metadata
- Transform metadata to a descriptive modern JSON schema
- Index metadata into ElasticSearch for easy exploration and search
- Import metadata into SQLite for advanced queries
- Document metadata before and after transformation

# Install

Download or clone this repository and install the following tools:

 - bash
 - curl
 - wget
 - [jq](https://stedolan.github.io/jq/)
 - [Xidel](https://www.videlibri.de/xidel.html)
 - elasticsearch-1.7.6

A configuration for ElasticSearch is provided in `srv/elasticsearch.yml`

# Usage

- `./download.sh`
- `./transform.sh`
- `./index.sh`

# Transform to GeoJSON for use with World Map

- `./GeoJSON.jq data/transformed.json > wmmsdb.geojson`

# Search Interface Setup

TODO
