#!/usr/bin/jq -fsc
# Build GeoJSON object from transformed JSON data
# Loaded by webroot/map.arcgis.html
#
# Usage: ./GeoJSON.jq data/transformed.json > data/wmmsdb.geojson
#
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2018-2022 leuc
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.
{
    "type": "FeatureCollection",
    "features": [
        .[] | select(.location.coordinates | length > 0) | {
            "type": "Feature",
            "geometry": {
                "type": "MultiPoint",
                "coordinates": .location.coordinates | map([.lon, .lat])
            },
            "properties": {
                "id": .record_number,
                "note": .note,
                "location_name": (.location.name | join(" ")),
                "observation_date": .observation_date,
                "species_common_name": .animal.species[].common_name,
                "species_scientific_name": .animal.species[].scientific_name,
            }
        }
    ]
}
