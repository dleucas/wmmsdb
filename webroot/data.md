% Technical notes on the content of the Watkins Marine Mammal Sound Database
%
% Last updated: December 22, 2021

## Download converted data

 - [v2019-06-01.b32a76a JSON](wmmsdb.20190601.b32a76a.json.xz) with added Geo Location Coordinates
 - [v2019-05-17 GeoJSON](wmmsdb.v20190517.geojson.xz) with Record Number, Location Description and Note as custom properties, used by the World Map.
 - [v2018-07-20 JSON](wmmsdb.v20180720.json.xz) with custom namespace, used by the search.

## Introduction

All database fields are described in 
[WHOI Technical Report 92-31: SOUND Database of Marine Animal Vocalizations - Structure and Operations][whoi.report].
This document will focus on the actual contents of the metadata provided by the WHOI website.

Note that the database is not available in it's native DOS based format, as created by INMAGIC 7.2 (INMAGIC INC., Cambridge, MA).

[whoi.report]: https://whoicf2.whoi.edu/science/B/whalesounds/WHOI-92-31.pdf

### Database Fields

(Still work in progress)

The following table describes the content of each database field. 
A field might contain multiple values and each value might encode multiple 
informations.

Each piece of information is normalized and mapped into a JSON object tree.
Abbreviations are translated into the full form.

Generally the database is very densely encoded with information due to the DOS-Area resource limitations.

| Field | Description               | Records | Missing | Unique | Multi-Valued | Transformed as                  |
|------:+:--------------------------+--------:+--------:+-------:+:------------:+:--------------------------------|
| RN    | Record Number             |   15254 |       0 |  15254 |           No | .record_number                  |
| CU    | Cue                       |         |      89 |        |           No | .signal.position.cue            |
| NC    | Number of Audio Channels  |         |       8 |     40 |           No | .sound.channel                  |
| SR    | Sample Rate               |         |       5 |     48 |           No | .sound.sample_rate              |
| CS    | Cut Size                  |         |      11 |   6131 |           No | .signal.cut_size                |
| PL    | Playback Gear             |         |       7 |    253 |           No | .playback.gear                  |
|       |                           |         |         |        |              | .playback.settings.bandpass_filter.high |
|       |                           |         |         |        |              | .playback.settings.bandpass_filter.low |
| SC    | Signal Class              |         |     952 |     26 |           No | .signal.quality                 |
|       |                           |         |         |        |              | .signal.class                   |
|       |                           |         |         |        |              | .signal.overlap                 |
| ID    | Vocal Animal ID           |         |   13338 |     18 |          Yes | .animal.vocal[].animal_id       |
|       |                           |         |         |        |              | .animal.vocal[].species_code    |
| AG    | Age                       |         |   14769 |     13 |          Yes | .animal.profile[].age           |
|       |                           |         |         |        |              | .animal.profile[].sex           |
|       |                           |         |         |        |              | .animal.profile[].animal_id     |
|       |                           |         |         |        |              | .animal.profile[].birth_year    |
| IA    | Interaction               |         |   15211 |      5 |          Yes | .animal.interaction[].type_of   |
|       |                           |         |         |        |              | .animal.interaction[].animal_id |
| GS    | Genus                     |         |       0 |    307 |          Yes | .animal.genus[].name            |
|       |                           |         |         |        |              | .animal.genus[].species_code    |
|       |                           |         |         |        |              | .signal.source[].name           |
|       |                           |         |         |        |              | .signal.source[].order          |
| GA    | Geo A Code                |         |      20 |    194 |          Yes |                                 |
| OD    | Observation Date          |         |       0 |    496 |          Yes | .observation_date               |
| NT    | Note                      |         |       4 |   5398 |           No | .note                           |
| DA    | Record Date               |         |      30 |    437 |           No | .last_modified_date             |
| IP    | ID of con present         |      15 |         |      2 |          Yes | | 
| BH    | [Behavior](#behavior)     |    2442 |         |     48 |          Yes | .animal.behavior[].type_of      |
|       |                           |         |         |        |              | .animal.behavior[].species_code |
| OS    | Other Species             |    3995 |         |     75 |          Yes | |
| NA    | Number of Animals Vocalizing | 14889 |        |    420 |          Yes | .animal.count[].from            |
|       |                           |         |         |        |              | .animal.count[].to              |
|       |                           |         |         |        |              | .animal.count[].species_code    |
| GB    | Geo B                     |   13354 |         |    362 |              | .location.name[]                |
| GC    | Geo C                     |   13910 |         |    224 |          Yes | .location.coordinates[]         |
| OT    | Observation Time          |    7141 |         |    450 |          Yes | .observation_time[]             |
| SH    | Ship                      |   13675 |         |     62 |           No | .ship                           |
| AU    | Author                    |   14204 |         |     58 |          Yes | .author                         |
| LO    | Storage location of original recording | | 16 |    134 |          Yes | .storage_location               |
| HY    | Hydrophone depth in m     |    8075 |         |     51 |          Yes | |
| RC    | Recording conditions      |    9524 |         |    126 |          Yes | .recording.conditions           |
| RG    | Recording gear            |   13046 |    2208 |    141 |          Yes | .recording.gear[]               |
| SL    | Signal level              |       1 |   15253 |      1 |           No | .signal.level                   |
| ST    | Signal type               |   13606 |    1648 |   2369 |          Yes | |

### Examples and Transformations

(WIP)

#### Behavior

> Behavior codes, species code.

Example values in the database

```
[...]
     52 Sperm whale surfaced 30m off stern, swam near stern phone and followed along | side of boat up to side hydrophone.
     59 Harpooned
     61 Feeding at surface   BD5A
     84 Bow riding  BD17A
     93 Bow riding  BD3B
     94 Milling  BD6A | Breaching  BD6A
    101 Feeding  BD19D
    127 Resting at surface  BD4A
    156 Social  BE7A
    166 Travelling  BD15A
    264 Milling  BD15C
    319 Courtship  AA1A
    408 BE7A
```

Note lines "Harpooned" and "BE7A", species code or behavior type is not always present.

Examples of `.animal.behavior[].type_of` after processing.

```
[...]
     20 Dive
     23 Foraging activity
     24 Socializing
     47 Surface feeding
     52 side of boat up to side hydrophone.
     52 Sperm whale surfaced 30m off stern, swam near stern phone and followed along
     59 Harpooned
     61 Feeding at surface
    135 Breaching
    156 Social
    163 Resting at surface
    178 Travelling
    191 Bow riding
    224 Feeding
    362 Milling
    366 Courtship
    408 null
```

Variants in writing ("Surface feeding", "Feeding at surface", etc) are not normalized.

