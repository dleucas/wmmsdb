# Current Progress

## Download metadata

 - (Done) Extract the list of mammals pages with xpath / xmllint
 - (Done) Download each mammal page and extract the list of years
 - (Done) Extract all retrieval numbers and download each metadata page
 - (TODO) use xidel instead of xmllint in download.sh
  
## Transform metadata

 - (Done) Extract the metadata as JSON from HTML
 - (Done) Review and document metadata for needed cleanup
 - (WIP) translate abbreviations, transform data, normalize
 - (WIP) design new data structure for more insight and useful queries

## Store and Index

 - (Done) Push data into ElasticSearch
 - (TODO) Define ElasticSearch field mapping (schema)
 - (TODO) Create a SQL schema
 - (TODO) Import data into SQL/SQLite3

## User Interface

 - (WIP) Allow filtering of all fields
 - (TODO) try something else then FacetView2 as a ES Web UI
