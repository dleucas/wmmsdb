#!/usr/bin/jq -fr

# jq filter chain to transform flat source metadata into object structure.
#
# Each transformation is it's own function with documented input examples.
#
#
# - Source data combines multiple values into one field with "|"
# - Use native JSON data types if possible
# - Clean-up whitespace and normalize value formats

# Mapping of species code to names
# Extracted from WHOI website by `download.sh`
import "./data/species.sci.names" as $species_sci_names;
import "./data/species.common.names" as $species_common_names;

#
# helper functions
#

def trim:
    # remove leading and trailing whitespace
    gsub("^\\s+|\\s+$";"");

def remove_non_digit:
    # remove any non-digit characters
    gsub("[^0-9]"; "");

def remove_extra_spaces:
    # replace two or more whitespaces with a single one
    sub("\\s{2,}"; " ");

def null_if_empty:
    if (. == "") then null
    elif (. | length  == 0) then null
    else . end;
#
# transform functions
#

# Convert Degree.Minute coordinates into decimal notation
def as_coord:
    # Example W073 or W70, degree only, negate
    if startswith("W") and length <= 4 then
        -(.[1:] | tonumber)
    # Example W12404 degree with minutes, negate
    # Negate after addition
    elif startswith("W") and length == 6 then
        -((.[1:4] | tonumber) + (.[4:] | tonumber / 60))
    # Example S38 degree only, negate
    elif startswith("S") and length == 3 then
        -(.[1:] | tonumber)
    # Degree with minutes, negate
    elif startswith("S") and length == 5 then
        -((.[1:3] | tonumber) + (.[3:] | tonumber / 60))
    # Degree only
    elif startswith("N") and length == 3 then
        (.[1:] | tonumber)
    # Degree with minutes N4439
    elif startswith("N") and length == 5 then
        ((.[1:3] | tonumber) + (.[3:] | tonumber / 60))
    # Degree only
    elif startswith("E") and length <= 4 then
        (.[1:] | tonumber)
    # Degree with minutes
    elif startswith("E") and length == 5 then
        ((.[1:3] | tonumber) + (.[3:] | tonumber / 60))
    else
        null
    end;

def as_date:
    (capture("^(?<date>\\d{1,2}-\\w{3}-\\d{4})") | .date | strptime("%d-%B-%Y") | todateiso8601)//null;

def as_observation_time:
    # Oservation time for original, species code
    # Somtime given as range of hours.
    #
    # Only extracting time, don't capture spaces and remove colon
    #
    # Example source data:
    # 10:10
    # 1010
    # 1345 - 1600  BD15A  X
    # - 1548BA2A
    # 1642-1700
    # 1753 - 1805  BD15A
    # 2300  BE7A  X
    #
    capture("(?<from_time>\\d{2}(:)?\\d{2})(\\s?\\-\\s)?(?<to_time>\\d{4})?")//false |
    if . then
    {
        "from_time": (.from_time | remove_non_digit),
        "to_time": .to_time
    }
    else null end;

def as_ship:
    # Ship/Cruise, aquarium, or other platform
    #
    # Database contains various ship names, sometimes species code and short codes.
    # Difficult to normalize with regex alone, manually fix the rest.
    #
    # Example source data:
    # Abel-J
    # ABEL J
    # ABEL-J
    # Annandale
    # Bear 265
    # BEAR 265
    # BE (captive in a 75' x 45' enclosure)
    # Hydrophone dangled from end of breakwater
    {
        "SH": "Ship",
        "SM": "Small Boat",
        "AQ": "Aquarium",
        "BE": "Beach",
        "IC": "Ice",
        "UN": "Unknown",
    } as $platform |
    # remove species code
    sub("\\b[A-C][A-Z]\\d{1,2}[A-Z]$"; "") |
    # clean spaces
    trim | remove_extra_spaces |
    # map short codes
    if . | length == 2 then $platform[.]
    # normalize special cases
    elif .[:5] == "Bear " then . | ascii_upcase
    elif . == "Abel-J" then "ABEL-J"
    elif . == "ABEL J" then "ABEL-J"
    elif . == "IDA Z" then "IDA-Z"
    elif . == "IDA- Z" then "IDA-Z"
    elif . == "Risk" then "RISK"
    elif . == "" then null
    else . end;

def as_author:
    # Author, originator of the recording.
    #
    # Various writings of the same names.
    # Clean spaces and correct the names.
    #
    # TODO various more unclear entries
    #
    # BBN (Gogas)
    # FWKF
    # Shiv
    # WES LO CD #1, 2
    {
        "Backus": "Richard H. Backus",
        "Rene Guy Busnel": "René-Guy Busnel",
        "Rene-Guy Busnel": "René-Guy Busnel",
        "W. C. Cummings": "William C. Cummings",
        "Paul Knapp, Jr.": "Paul A. Knapp Jr.",
        "M Podesta": "Michela Podestà",
        "Carleton Ray": "G. Carleton Ray",
        "G. Carlton Ray": "G. Carleton Ray",
        "Peter Tyack": "Peter L. Tyack",
        "Tyack": "Peter L. Tyack",
        "WAW": "William A. Watkins",
        "W.A.Watkins": "William A. Watkins",
        "William Watkins": "William A. Watkins",
        "Frank Watlington- Sofar": "Frank Watlington"
    } as $author_spelling | trim | remove_extra_spaces | $author_spelling[.?]//.;

def as_storage_location:
    # Storage location of original recording
    trim | remove_extra_spaces;

def as_location_name:
    # Location Name
    # Remove species code and whitespace
    #
    # Example source data:
    # 2.25 mi. west of Castle Rock, McMurdo Sound, Antarctica  CC5A
    #  20 mi. NW Gambell, St. Lawrence Island, Alaska  CC2A  X
    # Castle Harbour, Bermuda  AC2A
    #
    # TODO improve clean-up
    #  jq -r '.GB | split("|")[]' data/rn/*json| sort -u | grep -P '(\s+)?[A-D][A-Z]\d+[A-Z](\s+)?|([\sXO]*$)'
    gsub("(\\s+)?[A-D][A-Z]\\d+[A-Z](\\s+)?|(X$)"; ""; "gm");

def as_location_coordinates:
    # Example source data
    # N10BD15A  W086BD15A
    # N13BA2A  W061BA2A
    # N13BA2A  W061BA2A
    # N13BD15B  W061BD15B
    #  N14X  W061X
    # N75BB2A  W075BB2A  approx
    # S52BD1A  W070BD1A
    #  S71CC14A  E170CC14A
    map(capture("(?<lat>[NS]{1}\\d{1,4})[A-Z]{1,2}(\\d{1,2})?([A-Z]{1})?\\s+(?<lon>[EW]{1}\\d{1,5})")) |
    map({ lat: (.lat | as_coord), lon: (.lon | as_coord) });

def as_species_code:
    (capture("(?<code>[A-C][A-Z]\\d+[A-Z])") | .code)//null;

def as_species_common_name:
    as_species_code | $species_common_names[0][.?];

def as_species_sci_name:
    as_species_code | $species_sci_names[0][.?];

#
# Animal
#

def as_animal_interaction:
    # interaction between animals
    # always a pair, and multiple sets of pairs are possible
    # [[{}, {}]] or [[{} {}], [{} {}], ...]
    #
    # Example source data:
    # FCFB147  FCFB145
    # FCFB147  FCFB145 | FFFB147  FFFB149 | FFFB145  FFFB149 | FCFB153  FCFB150
    # FCFB153  FCFB150
    # FCFB5  FCFB55
    # FCFB73  FCFB34
    capture("(?<type_of>[FMC]{2})(?<animal_id>FB\\d+)"; "g")//null;

def as_animal_profile:
    # age, sex and id, a animal profile
    # ignoring species code
    #
    # Example source data:
    # F03FB55  F1986FB55
    # F26FB5  F1963FB5
    # F??FB145  F????FB145
    # F??FB147  F????FB147
    # F??FB153  F????FB153
    # F??FB73  F????FB73
    # F??FB73  F????FB7370
    # M05FB150  M1984FB150
    # M17Keiko  M1975Keiko  BE7A
    # M17Keiko  M1975Keiko BE7A
    # M??FB34  M????FB34
    # M??FB73  M????FB73
    capture("^(?<sex>[FM])" +
            "(?<age>[\\?\\d]{2})" +
            "(?<animal_id>(FB\\d+|\\w+))" +
            "\\s+" +
            "[FM](?<birth_year>[\\d\\?]{4})")//null |
    {"F": "Female", "M": "Male"} as $sex |
    {
        sex: (if (.sex != null) then $sex[.sex] else null end),
        age: (try (.age | tonumber) catch null),
        animal_id: .animal_id,
        birth_year: (try (.birth_year | tonumber) catch null)
    };

def as_animal_behavior:
    # Behavior of the recorded animal with species code
    # species code not always present, use input as fallback
    #
    # Example source data:
    # Approaching ship  BA2A
    # BA2A  A few larger whales seen mixed with others
    # BE7A
    # Bow riding  BD17A
    # Courtship  CB1A
    # Dive  BA2A
    # Feeding  AA3A

    # find the species code position and use the text before as behavior
    # match() returns "empty" which we can not test with if
    . as $b | match("[A-C][A-Z]\\d+[A-Z]([\\s\\.]+)?$"; "m")//false |
    if . then
    {
        type_of: ($b[0:.offset] | trim | if (.|length) > 0 then . else null end),
        species_code: .string | trim
    }
    # fallback without species code
    else
        { type_of: $b | trim }
    end;

def as_animal_vocal:
    # List of vocal animals, name and species code
    # All existing entries:
    # FB145  #??  BD19D
    # FB147  #??  BD19D
    # FB150  #??  BD19D
    # FB153  #50  Blacktip Doubledip  BD19D
    # FB34  #30  Wee Willie  BD19D
    # FB55  #159  BD19D
    # FB5  #5  BD19D
    # FB73  #35  BD19D
    # Keiko
    # Keiko  BE7A
    # Minks  BF2A | Jinks  BF2A
    # Moby Doll
    # Moby Doll  BE7A
    # Olaf  CB1A
    # Snoopy  BA2A
    # The lark  BE3B
    # Wolfie  CB1A | Farouk  CB1A
    #
    # create array with objects for each animal
    # save input as fallback and split by |
    . as $input | $input | split("|") |
    # try to match species code
    map(. as $s | match("[A-C][A-Z]\\d+[A-Z](\\s+)?$"; "m") |
    # create object, anything before matched species code is id
    # also trim space from resulting string
    {
        animal_id: $s[0:.offset] | trim,
        species_code: .string | trim,
        scientific_name: $s | as_species_sci_name,
        common_name: $s | as_species_common_name,
    }) |
    # if no object was created, use input as fallback
    # this is for entries without a species code like "Keiko"
    if (. == [] and ($input|length)>0 ) then [{animal_id: $input}] else . end;

def as_animal_genus:
    map(. as $s | match("[A-C][A-Z]\\d+[A-Z](\\s+)?$"; "m") |
    {
        name: $s[0:.offset] | trim,
        species_code: .string | trim
    });

def as_animal_species:
    map(. as $s |
    {
        _as_noted: $s | trim,
        species_code: $s | as_species_code,
        scientific_name: $s | as_species_sci_name,
        common_name: $s | as_species_common_name,
    });

def as_animal_count:
    trim | capture("^(?<from>\\d+)\\s?[\\-\\+]?\\s?(?<to>\\d)?(\\s|$)(?<species_code>[A-C][A-Z]\\d+[A-Z]$)?")//null;

#
# Signal
#

# Cue field contains 3 values describing the postion on tape
# Example input from the docu
# 542 B2:8 8.130
# 1:03:12 B2:8 8.130
# however, following formats are also found
# 0:00:00  B30:00  10:20.602
# 995  B11:28.497  5:20.426
# 96  B4.00  1.525
# 93 B23.7  9.164
# 93  B3:00  2:13.828
# 01:52:52:04
# 09:11:00  20:00  951.50
# 0  B2:00:00

def as_signal_position_cue:
    # "cue" as in a first matched single integer,
    # without dot or colon followed by space or end of string
    # do not use \b because of the colon in 00:00 values
    capture("(?<c>^\\d+(\\s|$))") | {"cue": (.c | tonumber)};

def as_signal_position_time:
    # "time" as in first matched integer with 2 or 3 colons
    # followed by space or end of string
    capture("(?<time>^\\d+:\\d+:\\d+(:\\d+)?(\\s|$))");

def as_signal_position_analyzer_buffer_size:
    # buffer size, B followed by integer with colon or dot,
    # also remove B prefix
    # TODO match 2 colon version
    capture("(?<analyzer_buffer_size>(?<=B)\\d+[:\\.]\\d+(\\.\\d+)?)");

# Signal class encodes multiple values, quality, overlap and class
#
# it's only been used 123 times
#
# Example source data:
# 3  OT
# 3  OTF
# C 4  OF
# D
# M
# N
# No
# NO
# OF
# OT
# OTF
# OTF 3
# OTF 4
# S
# S 5
# U
# V

def as_signal_quality:
    # any digit in the signal class indicates quality
    (capture("(?<q>\\d+)") | .q | tonumber)//null;

def as_signal_class:
    # class name lookup table
    ({
        "S": "Signature",
        "M": "Mimic",
        "V": "Variant",
        "D": "Deletion",
        "U": "Uncharacteristic",
        "C": "Calf"
    } as $class_names | capture("(?<c>[SMVDUC]{1})") | $class_names[.c]?)//null;

def as_signal_overlap:
    ({
        "OF": "Frequency",
        "OT": "Time",
        "OTF": "Time and Frequency",
        "N": "No"
    } as $overlap_type | capture("(?<o>O[TF]{1,2}|N)") | $overlap_type[.o]?)//null ;

def as_signal_cut_size:
    # Signal cut size
    #
    # Example source data:
    # 3.36
    # 9.411
    # 16.564
    # 20.35
    # etc
    # only 210 records use a different format, ignored for now
    # 2:00.000
    # 1:00.030
    # 10:25.540
    # 1:25.158
    # etc.
    # set to null if empty or contains a colon
    if (. | contains(":") or (length == 0)) then
        null
    else
        # cast as number and handle a few remaining badly formated
        # records like "0.2.95"
        (try (. | tonumber) catch null)
    end;

def as_signal_source:
    # Other general sound producing sources listed in genus field
    #
    # Example source data:
    #  Transient ship noise  X
    # Ship electrical noise  X
    #  Rain  X
    #  Homo sapiens  E
    # Crustacea  O
    map(. as $s | match("\\s+[E-Z]{1}(\\s+)?$"; "m") |
    {
        "E": "Primates",
        "O": "Crustacea",
        "T": "Fossils",
        "U": "Uncertain",
        "V": "General pinniped",
        "W": "General cetacean",
        "X": "Ambient noise"
    } as $order |
    {
        name: $s[0:.offset] | trim,
        # not sort order
        order: $order[.string | trim]
    });

def as_signal_type:
    # Signal type, documented as:
    #
    # long => 0.1sec
    # short <= 0.1sec
    #
    # BL Broadband long, noisy, manu frequencies
    # BS Broadband short, clicks, pulses
    # NL Narrowband long, tonal, few harmonics
    # NS Narrowband short, bell, short tones
    # FM Frequency modulated, sweeps, "contours"
    # CH Chirp, shot FM sweep
    # PU Pulsed, click burts, sidebands
    # SE Series, train, sequence of sounds
    # SO Song, long, repetetive, patterned
    #
    # -- with species code. Example: NLAB1A
    #
    # /2 Signal type, general: coda, slow clicks, etc.
    # -- with species code.
    #
    # Lots of variations in writing
    #
    # Example source data:
    # Whistles BD3B
    # Click BE3C
    # Clicks BD15A
    # NLBE7A
    # Squeal BE7A
    # Clicks BD3B
    # Ship noise X
    # BSBD3B
    # Clicks BA2A
    # BSBD15A
    # FMBE7A
    # PUBE7A
    # FMBD3B
    # Whistle BE3C
    # Ship engine noise X
    # BLX
    # BSBE3C
    # FMBE3C
    # NLX
    # BSBA2A
    # BSX
    #
    # Distribution for short codes:
    #    159 SO
    #    276 CH
    #    300 NS
    #    508 SE
    #    625 BL
    #   3600 NL
    #   3885 PU
    #   7559 FM
    #   8186 BS
    #  31588 null
    {
        "BL": "Broadband long",
        "BS": "Broadband short",
        "NL": "Narrowband long",
        "NS": "Narrowband short",
        "FM": "Frequency modulated",
        "CH": "Chirp",
        "PU": "Pulsed",
        "SE": "Series",
        "SO": "Song"
    } as $type_codes |
    trim | remove_extra_spaces | . as $t |
    # capture type code
    capture("^(?<type_code>BL|BS|NL|NS|FM|CH|PU|SE|SO)(?<species_code>[A-D][A-Z]\\d{1,2}[A-Z]|[XxOoUu])$")//false |
    # fallback to text before species code
    if . == false then
        ($t | capture("(?<type_name>.*?)\\s*(?<species_code>[A-C][A-Z]\\d{1,2}[A-Z]|[XxOoUu])$")//null)
    else
        .type_name = $type_codes[.type_code]
    end;

def as_sound_channel:
    # numbers of channels
    # input data mostly follows the documentation:
    # 11A
    # 11B
    # 41D
    # 21L
    # regex will match only those
    # not clear what other input values mean exactly
    # 211
    (capture("^(?<r>\\d)(?<m>\\d)(?<s>[A-L]$)") |
    {
        recorded: .r | tonumber,
        multiplexed: .m | tonumber,
        side: .s
    })//null;


def as_sound_sample_rate:
    # plain sample rate as number
    # remove dot or colon, and ignore empty strings
    #
    # Example source data:
    # 1000
    # 10,000
    # 10000
    # 100000
    # 10200
    if (. | length > 0) then . | sub("[\\.,]"; "") | tonumber else null end;

def as_recording_conditions:
    # Conditions, weather, salinity
    #
    # Only removing a few species codes. Not useful as filter
    #
    # Example source data:
    #
    # Cruise ship in area, wind 10 kts. WNW, seas 2-3ft.
    # Flat, calm, sunny, occassional boats
    # High generator background RG AX-58 hydrophones
    # In air CC12F
    # Magnecorder tape recorder
    # On ice. Noisy ship background   CC12F
    sub("\\b[A-C][A-Z]\\d{1,2}[A-Z]$"; "") | trim | remove_extra_spaces;

def as_recording_gear:
    # Recording gear, equipment
    #
    # Often a list of items, splitting by | , and ;
    #
    # Example source data
    #
    # Suitcase Mod 3 #3 amplifier | AX-58 hydrophone | Crown #8 'F' equalization | Scotch III-A Tape
    # TEAC DA-P20 Digital Recorder
    # Teac DAT Recorder, pole buoy hydrophone
    # Panasonic AG-6400 VHS, 2 Hi-Tech HTI-90-SSQ hydrophones | 1 WHOI Video camera
    # Ithaco 602M108 hydrophones ; Ithaco 450 amplifiers | WHOI/PEMTEK tape recorders
    # AX-58 hydrophones, WHOI suitcase amplifier | WHOI transistor amplifier, Crown tape recorder
    split("[|,;]"; "") | map(trim | remove_extra_spaces | null_if_empty);

def as_playback_settings:
    # Playback gear used during transfer to digital?
    #
    # filter type settings in khz (pass-band)
    # L = low (high-pass) and
    # H = high (for low-pass)
    # Example: PEMTEK/KROHN-HITE L0.1 H12
    #
    # K-H / Krohn-Hite was the bandpass filter device used?
    #
    # Extract filter setting
    #
    # Example source data:
    # 4-ch Crown/ K-H L0.24
    # Crown-4CH/ FDL20 / FDH2
    # Crown-4CH/K-H  H6.0  L0.15
    # Crown/ Kay/  K-H H4.81
    # Crown/K-H L 0.95kHz, H 38.0kHz
    # JVC BR-7700U/ K-H L3/  Kay
    # JVC SVHS VCR, K-H L0.6 H12.0
    # Pemtek-4CH/K-H LP28.0kHz HP93.0kHz
    # Pemtek LP K-H 93kHz, HP K-H 100Hz
    # JVC VCR /K-H L0.8 H10.0
    # Pemtek 301, K-H L2.5 H50.0
    # Pemtek 110/K-H H3.1KHz L28KHz
    # Sony DAT recorder 7700
    # Pemtek 301/ K-H L0.02 L50.0
    {
        bandpass_filter: {
            low: (capture("L(?<low>\\d+(\\.\\d+)?)(\\s?[Kk][Hh]z)?")//null | .low?),
            high: (capture("H(?<high>\\d+(\\.\\d+)?)(\\s?[Kk][Hh]z)?")//null | .high?)
        }
    };

def as_playback_gear:
    # see as_playback_settings
    #
    # Few devices but many variants in writing. Test and normalize.
    #
    if test("crown\\W?4\\W?ch|4\\-ch\\scrown"; "i") then "Crown 4 Channel Tape Recorder"
    elif test("crown\\W?2\\W?ch"; "i") then "Crown 2 Channel Tape Recorder"
    elif test("^crown"; "i") then "Crown Tape Recorder"
    elif test("^sharp"; "i") then "Sharp RT-1010 Stereo Cassette Deck"
    elif test("^pemtek\\s1[10]0"; "i") then "Pemtek 110 Tape Recorder"
    elif test("^pemtek\\s3[10]+"; "i") then "Pemtek 301 Tape Recorder"
    elif test("^pemtek\\W4\\W?ch"; "i") then "Pemtek 4 Channel Tape Recorder"
    elif test("^pemt(ek)?"; "i") then "Pemtek Take Recorder"
    elif test("^Sony\\sDAT"; "i") then "SONY PCM-E7700 DAT-Station"
    elif test("^JVC"; "i") then "JVC BR-7700U S-VHS VCR"
    elif test("^Marantz\\scassette"; "i") then "Marantz SD 420 Stereo Cassette Deck"
    elif test("^Superscope"; "i") then "Superscope Cassette Recorder"
    elif test("^yam"; "i") then "Yamaha?"
    elif test("^Toshiba"; "i") then "Toshiba 8X IDE CD-ROM; Ensoniq Soundscape VIVO Wavetable"
    else null end; # remaining entries had no gear listed

#
# Assemble the object tree
#

# root
{
    # record number is unique, can be used as _id
    record_number: .RN,
    note: .NT,
    # a lot of noise in the "OD" original field, only parsing date
    observation_date: .OD | as_date,
    observation_time: .OT | split("|") | map(as_observation_time),
    last_modified_date: .DA | as_date,
    location: {
        name: .GB | split("|") | map(as_location_name),
        coordinates: .GC | split("|") | as_location_coordinates
    },
    ship: .SH | as_ship,
    author: .AU | split("|") | map(as_author),
    storage_location: .LO | split("|") | map(as_storage_location),
    # recording setup
    recording: {
        # mostly single values, join the few multi-values
        "conditions": .RC | split("|") | map(as_recording_conditions) | join(", ") | null_if_empty,
        "gear": .RG | as_recording_gear
    },
    playback: {
        "settings": .PL | as_playback_settings,
        "gear": .PL | as_playback_gear,
    },
    # object contains properties of the captured signal
    signal: {
        position: [
            # keep the source string as reference?
            {_source_cu: .CU},
            (.CU | as_signal_position_cue),
            (.CU | as_signal_position_time),
            (.CU | as_signal_position_analyzer_buffer_size )
        ] | add,
        cut_size: .CS | as_signal_cut_size,
        _source_sc: .SC,
        quality: .SC | as_signal_quality,
        class: .SC | as_signal_class,
        overlap: .SC | as_signal_overlap,
        source: .GS | split("|") | as_signal_source,
        # Documented as "Signal level, _R_eceived, _S_ource, (db), species code.
        # _E_stimated. Example: S172AB1A E
        #
        # However, WHOI website has only one (wrong) entry here: "Atlantic Research LC32 hydrophone"
        # Maybe data loss during conversaion from DOS database?
        level: (if .SL == "" then null else .SL end),
        type_of: .ST | split("|") | map(as_signal_type)
    },
    sound: {
        sample_rate: .SR | as_sound_sample_rate,
        channel: [
            {"_source_nc": .NC },
            (.NC | as_sound_channel)
        ] | add
    },
    animal: {
        _source_id: .ID,
        # List of vocal animals, name and species code
        vocal: .ID | as_animal_vocal,
        # age, sex and id, a animal profile
        profile: .AG | as_animal_profile,
        # interaction between animals
        interaction: .IA | split("|") | map([as_animal_interaction]),
        # species code not always present, use input as fallback
        behavior: .BH | split("|") | map(as_animal_behavior),
        # Genus name and species code
        # genus: .GS | split("|") | as_animal_genus,
        # Species
        species: .GS | split("|") | as_animal_species,
        # numbers of vocal animals
        count: .NA | split("|") | map(as_animal_count),
    }
}
