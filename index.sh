#!/bin/bash
# create JSON for ElasticSearch _bulk interface and pipe it directly in
# --raw-output --compact-output matter or ES won't accept it

CURL="curl -s"
ES=localhost:9200
IDX=wmmsdb

$CURL -XDELETE $ES/$IDX | jq .
$CURL -XPUT $ES/$IDX | jq .
$CURL -XPUT $ES/$IDX/_mapping/record -d@index.mapping.json | jq .

jq --raw-output --compact-output -f index.jq data/transformed.json | $CURL -s -H "Content-Type: application/x-ndjson" -XPOST $ES/_bulk --data-binary "@-" | jq .took
