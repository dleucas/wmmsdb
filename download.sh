#!/bin/bash
#
# Scrape metadata from Woods Hole Oceanographic Institution
#
# Download of all audio files is left as exercise to the reader
#
# - Grab species index from "All Cuts" page
# - Grab all audio cuts for every species for all listed years
# - Grap metadata pop-up for every audio cut
#
# https://whoicf2.whoi.edu/science/B/whalesounds/metaData.cfm?RN=91008005
#
# Using xmllint XPath 1.0 for parsing, because it continues on broken HTML.
#
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright (C) 2021 leuc
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.

set -e

test -e "$(command -v wget)" || (
  echo "ERR: Please install wget"
  exit 1
)
test -e "$(command -v xmllint)" || (
  echo "ERR: Please install xmllint from libxml2-utils"
  exit 1
)

URL='https://cis.whoi.edu/science/B/whalesounds'
WGET='wget -nv -nc'
XMLLINT='xmllint --recover --html'

# Ensure target dir
test -d raw || mkdir raw
test -d data || mkdir data

# Download Index page listing all search options
$WGET "$URL/fullCuts.cfm" -O raw/fullCuts.cfm || true

# Extract catalog IDs for each mammal from HTML drop down
XPATH_SP='//select[@id="getSpecies"]/option[not(contains(text(),"Select"))]/@value'
$XMLLINT -xpath "${XPATH_SP}" raw/fullCuts.cfm 2>/dev/null | grep -oP 'SP=\K(\w+)' >data/species.list

# Extract mammal names from HTML drop down
XPATH_NAME='//select[@id="getSpecies"]/option[not(contains(text(),"Select"))]/text()'
$XMLLINT --xpath "${XPATH_NAME}" raw/fullCuts.cfm 2>/dev/null | sed '/^\s*$/d' | sed 's/^\s*//g' | sed 's/\s*$//g' >data/species.names

# Make unique list of IDs
sort -u data/species.list >data/species.ids

# Merge ids and names for later mapping in transform.sh
paste data/species.list data/species.names >data/species.map

# create a list of pages to download
while read -r SP; do
  echo "$URL/fullCuts.cfm?SP=$SP&YR=-1"
done <data/species.ids >data/species.urls

# Download all pages
$WGET -P raw/sp/ -i data/species.urls

# create a list of pages for each year and species
XPATH_YR='//select[@id="pickYear"]/option/@value'
while read -r SP; do
  YEARS=$($XMLLINT -xpath "$XPATH_YR" "raw/sp/fullCuts.cfm?SP=$SP&YR=-1" 2>/dev/null | grep -oP 'YR=\K(\d+)' | sort -u)
  for YEAR in $YEARS; do
    echo "$URL/fullCuts.cfm?SP=$SP&YR=$YEAR"
  done
done <data/species.ids >data/species.year.urls

$WGET -P raw/spyr/ -i data/species.year.urls

# Extract retrieval number from all sp/year pages
XPATH_RN='//table//tr//td[5]/a/@href'
for F in raw/spyr/fullCuts.cfm*; do
  $XMLLINT -xpath "$XPATH_RN" "$F" 2>/dev/null | grep -oP 'WhaleSounds/\K([\da-zA-Z]+)'
done | sort -u >data/retrieval.numbers

# Create list of URLs to download
while read -r RN; do
  echo "$URL/metaData.cfm?RN=$RN"
done <data/retrieval.numbers >data/retrieval.urls

$WGET -P raw/rn/ -i data/retrieval.urls
