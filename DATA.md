Notes on the data

Total number of records 15254

RN: Always present, all unique 
CU / Cue: 89 missing, 12631 contain "B" buffer size flag
NC / channels: 8 missing, 40 unique values, some invalid format
SR / sample rate: 5 missing, 48 unique values, some with . delimiter, mixed kHz Hz writing
CS / cut size: 11 missing, 6131 values, mostly seconds (n.n+), some with minutes (n:n.n+)?
PL / recorder: 7 missing, 253 unique values
SC / signal class, 952 missing, 26 unique values, quality not always present, flags in no order
ID / vocal animal id: 13338 missing, 18 unique values, species code not always present
AG / age: 14769 missing, 13 unique values, using ? as placeholder if age is unknown, species code might be name
IA / interaction: 15211 missing, 5 unique values, multiple interaction with | separated, always pairs
GS / genus: always present, 307 unique values, | separated, other species codes X / O / E
GA / Geo A code: 20 missing, 194 unique values, | separated
OD / observation date, always present, 496 unique values, | separated
NT / note: 4 missing, 5398 unique values, free text
DA / record date: 30 missing, 437 unique values, Month written 3 or 4 letters, some extra noise
IP / ID of con present: 15 records, 2 unique values, | separated
AG / age of con present: 15 records, 2 unique values
BH / behavior: 2442 records, 48 unique values, some variation/free text, normalize?
OS / other species: 3995 records, 75 unique values, | separated, not vocalizing species?
NA / number of animals vocalizing: 14889 records, 420 unique values, ranges 1-2, or 1+, handle space, | separated, some noise
GB / Geo B: 13354 records, 362 unique values
GC / Geo C: 13910 records, 224 unique values, | separated
OT / observation time: 7141 records, sometimes range nnnn - nnnn, | or ; separated
SH / ship: 13675 records, 62 unique values
AU / author: 14204 records, 58 unique values, | separated
LO / storage location: 16 missing
HY: 8075 missing
RC: 9524 missing
RG: 2208 missing
SL: 15253 missing
ST: 1648 missing
